package com.exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabExerciseApplication.class, args);
    }

}
